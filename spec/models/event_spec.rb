require 'rails_helper'

RSpec.describe Event, type: :model do
  describe 'validations and assotiations' do
    it { should validate_presence_of :event_type }
    it { should validate_inclusion_of(:event_type).in_array(Event::AVAILABLE_TYPES) }
    it { should belong_to :url }
  end
end
