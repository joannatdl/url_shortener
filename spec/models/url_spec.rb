require 'rails_helper'

RSpec.describe Url, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:original_url) }
    it { is_expected.to validate_url_of(:original_url) }
    it { is_expected.to validate_presence_of(:slug) }
    it { is_expected.to validate_uniqueness_of(:slug) }

    context 'invalid slug' do
      before { subject.slug = 'invalid slug!' }
      it { is_expected.to be_invalid }
    end
  end
end
