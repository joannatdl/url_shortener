require 'rails_helper'

RSpec.describe GetUrlCsvStats, type: :service do
  subject { GetUrlCsvStats.call }

  describe '.call' do
    let!(:event) { create(:event) }

    it 'succeeds' do
      expect(subject).to be_a_success
    end

    it 'provides csv stats' do
      rows = subject.stats.split("\n")
      headers = rows.shift.split(',')
      data_rows_count = rows.count
      data_rows_string = rows.first
      expect(headers).to match_array(GetUrlCsvStats::HEADERS)
      expect(data_rows_count).to eq(1)
      expect(data_rows_string).to eq(GetUrlCsvStats::HEADERS.map { |attr| event.url.public_send(attr) }.join(','))
    end

    it 'provides file name with current date' do
      expect(subject.file_name).to eq("urls-stats-#{Date.today}.csv")
    end
  end
end
