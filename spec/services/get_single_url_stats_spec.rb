require 'rails_helper'

RSpec.describe GetSingleUrlStats, type: :service do
  subject { GetSingleUrlStats.call(slug_params) }
  let(:slug) { Faker::Internet.slug }
  let(:slug_params) { { slug: slug } }

  describe '.call' do
    context 'url is found in the database' do
      let(:url) { build(:url) }
      before do
        allow(Url).to receive(:find_by).and_return(url)
        allow(url).to receive(:enters_count).and_return(5)
      end

      it 'succeeds' do
        expect(subject).to be_a_success
      end

      it 'provides the url' do
        expect(subject.url).to be_an(Url)
      end

      it 'assigns stats' do
        expect(subject.stats).to eq({ enters_count: 5 })
      end
    end

    context 'url is not found in the database' do
      before { allow(Url).to receive(:find_by).and_return(nil) }

      it 'fails' do
        expect(subject).to be_a_failure
      end

      it 'provides a failure message' do
        expect(subject.message).to eq("URL for slug '#{slug}' was not found")
      end
    end
  end
end
