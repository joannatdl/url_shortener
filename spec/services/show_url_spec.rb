require 'rails_helper'

RSpec.describe ShowUrl, type: :service do
  subject { ShowUrl.call(params) }

  describe '.call' do
    let(:params) { { slug: slug, ip: Faker::Internet.public_ip_v4_address } }
    let(:slug) { Faker::Internet.slug }

    context 'url is found in the database' do
      let(:event) { build(:event, url: url) }
      let(:url) { build(:url, slug: slug) }
      before { allow(Url).to receive(:find_by).and_return(url) }

      context 'event was saved in the database' do
        before { allow_any_instance_of(Event).to receive(:save).and_return(true) }

        it 'succeeds' do
          expect(subject).to be_a_success
        end

        it 'provides the url' do
          expect(subject.url).to be_an(Url)
        end

        it 'message is empty' do
          expect(subject.message).to be_empty
        end
      end

      context 'event was not saved in the database' do
        before { allow_any_instance_of(Event).to receive(:save).and_return(false) }

        it 'succeeds' do
          expect(subject).to be_a_success
        end

        it 'provides the url' do
          expect(subject.url).to be_an(Url)
        end

        it 'message is empty' do
          expect(subject.message).to be_empty
        end

        pending 'logs failure about not saving event'
      end
    end

    context 'url is not found in the database' do
      before { allow(Url).to receive(:find_by).and_return(nil) }

      it 'fails' do
        expect(subject).to be_a_failure
      end

      it 'provides a failure message' do
        expect(subject.message).to eq("URL for slug '#{slug}' was not found")
      end
    end
  end
end
