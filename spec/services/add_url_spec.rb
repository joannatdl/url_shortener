require 'rails_helper'

RSpec.describe AddUrl, type: :service do
  subject { AddUrl.call(params) }

  describe '.call' do
    let(:valid_url) { Faker::Internet.url }
    let(:params) { { original_url: valid_url } }

    context 'save succeeds' do
      before do
        allow_any_instance_of(Url).to receive(:save).and_return(true)
      end

      context 'slug is provided' do
        let(:params) { { original_url: valid_url, slug: slug } }
        let(:slug) { Faker::Internet.slug }

        it 'succeeds' do
          expect(subject).to be_a_success
        end

        it 'uses the slug to create url' do
          expect(subject.url.slug).to eq(slug)
        end
      end

      context 'slug is not provided' do
        it 'provides the url' do
          expect(subject.url).to be_an(Url)
        end

        it 'provides a success message' do
          expect(subject.message).to eq('URL was successfully added')
        end

        it 'succeeds' do
          expect(subject).to be_a_success
        end
      end
    end

    context 'save fails' do
      before do
        allow_any_instance_of(Url).to receive(:save).and_return(false)
      end

      it 'fails' do
        expect(subject).to be_a_failure
      end

      it 'provides a failure message' do
        expect(subject.message).to eq('Could not add URL')
      end

      it 'provides the url' do
        expect(subject.url).to be_an(Url)
      end
    end
  end
end
