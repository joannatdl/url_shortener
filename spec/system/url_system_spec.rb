require 'rails_helper'

RSpec.describe 'URL management', type: :system, js: true do
  let(:slug) { Faker::Internet.slug }

  context 'Adding an URL' do
    let(:valid_url) { Faker::Internet.url }
    let(:invalid_url) { 'http://example .com' }

    scenario 'with valid data' do
      visit '/'
      fill_in 'Type URL...', with: valid_url
      fill_in 'Type your slug here or leave blank to get a random one...', with: slug
      click_button 'Add'

      expect(page).to have_text('URL was successfully added')
    end

    scenario 'without a slug' do
      visit '/'
      fill_in 'Type URL...', with: valid_url
      click_button 'Add'

      expect(page).to have_text('URL was successfully added')
    end

    scenario 'with invalid data' do
      visit '/'
      fill_in 'Type URL...', with: invalid_url
      fill_in 'Type your slug here or leave blank to get a random one...', with: slug
      click_button 'Add'

      expect(page).to have_text('is not a valid URL')
    end
  end

  context 'Accessing an added slug' do
    let!(:url) { create(:url) }

    scenario 'redirects to its original URL' do
      get "/#{url.slug}"
      expect(response).to redirect_to(url.original_url)
    end
  end

  context 'Accessing a slug that was not added' do
    scenario 'shows an alert' do
      visit "/#{slug}"
      expect(page).to have_text("URL for slug '#{slug}' was not found")
    end
  end

  context 'Accessing stats for an added slug' do
    let!(:url) { create(:url) }

    scenario 'shows stats' do
      visit "/#{url.slug}/stats"
      expect(page).to have_text('stats')
    end
  end

  context 'Accessing stats for a slug that was not added' do
    scenario 'shows an alert' do
      visit "/#{slug}/stats"
      expect(page).to have_text("URL for slug '#{slug}' was not found")
    end
  end

  context 'Revisiting stats after entering a link' do
    let!(:url) { create(:url) }

    scenario 'properly increments visits number' do
      visit "/#{url.slug}/stats"
      expect(page).to have_text('Visited 0 times')
      get "/#{url.slug}"
      visit "/#{url.slug}/stats"
      expect(page).to have_text('Visited 1 time')
    end
  end
end
