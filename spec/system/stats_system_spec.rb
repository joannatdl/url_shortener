require 'rails_helper'

RSpec.describe 'Stats', type: :system do
  after(:each) { File.delete(file_name) if File.exist?(file_name) }
  let!(:event) { create(:event) }

  scenario 'Getting CSV stats', driver: :rack_test do
    visit '/stats.csv'
    expect(page.response_headers['Content-Type']).to eq('text/csv')
    header = page.response_headers['Content-Disposition']
    expect(header).to match(/^attachment/)
    expect(header).to match(/filename="#{file_name}"/)
  end
end

def file_name
  "urls-stats-#{Date.today}.csv"
end
