FactoryBot.define do
  factory :url do
    original_url { Faker::Internet.url }
    slug { Faker::Internet.slug }
  end
end
