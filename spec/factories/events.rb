FactoryBot.define do
  factory :event do
    event_type { Event::AVAILABLE_TYPES.sample }
    user_data  { { ip: Faker::Internet.public_ip_v4_address, country_code: Faker::Address.country_code } }
    url
  end
end
