class ShowUrl < ApplicationService
  attr_reader :url

  def initialize(params)
    @slug = params[:slug]
    @ip = params[:ip]
  end

  def call
    find_url_by_slug
    increment_url_stats
    set_message
    set_success
    success?
  end

  private

  def find_url_by_slug
    @url = Url.find_by(slug: @slug)
  end

  def increment_url_stats
    Event.create(url: @url, event_type: :url_entered, user_data: user_data) if @url
  end

  def set_message
    @message = @url ? '' : "URL for slug '#{@slug}' was not found"
  end

  def set_success
    @success = @url.present?
  end

  def user_data
    {
      ip: @ip,
      country_code: country_code
    }
  end

  def country_code
    :pl # TO DO use an API to get country code from IP
  end
end
