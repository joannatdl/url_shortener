class ApplicationService
  attr_reader :message

  def self.call(*args, &block)
    service = new(*args, &block)
    service.call
    service
  end

  def success?
    !!@success
  end

  def failure?
    !success?
  end
end
