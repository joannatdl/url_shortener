class AddUrl < ApplicationService
  attr_reader :url

  def initialize(url_params)
    @slug = url_params[:slug]
    @original_url = url_params[:original_url]
  end

  def call
    ensure_slug
    save_url
    set_message
    success?
  end

  private

  def ensure_slug
    @slug = Faker::Internet.slug unless @slug.present?
  end

  def save_url
    @url ||= Url.new(slug: @slug, original_url: @original_url)
    @success = @url.save
  end

  def set_message
    @message = success? ? 'URL was successfully added' : 'Could not add URL'
  end
end
