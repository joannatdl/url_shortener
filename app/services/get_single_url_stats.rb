class GetSingleUrlStats < ApplicationService
  attr_reader :url, :stats

  def initialize(slug_params)
    @slug = slug_params[:slug]
  end

  def call
    find_url_by_slug
    set_stats
    set_success
    set_message
    success?
  end

  private

  def find_url_by_slug
    @url = Url.find_by(slug: @slug)
  end

  def set_stats
    @stats = { enters_count: @url.enters_count } if @url
  end

  def set_success
    @success = @url && @stats.present?
  end

  def set_message
    @message = "URL for slug '#{@slug}' was not found" and return unless @url

    @message = success? ? '' : 'Count not create stats'
  end
end
