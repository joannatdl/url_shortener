require 'csv'

class GetUrlCsvStats < ApplicationService
  HEADERS = %w[id slug original_url enters_count created_at].freeze

  attr_reader :stats, :file_name

  def call
    @stats = urls_as_csv
    @file_name = "urls-stats-#{Date.today}.csv"
    @success = true
  end

  private

  def urls_as_csv
    CSV.generate(headers: true) do |csv|
      csv << HEADERS

      Url.all.find_each do |url|
        csv << HEADERS.map { |attr| url.send(attr) }
      end
    end
  end
end
