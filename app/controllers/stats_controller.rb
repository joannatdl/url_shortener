class StatsController < ApplicationController
  def index
    respond_to do |format|
      format.csv do
        result = GetUrlCsvStats.call
        send_data result.stats, filename: result.file_name
      end
    end
  end
end
