class UrlsController < ApplicationController
  def new
    @url = Url.new
  end

  def create
    result = AddUrl.call(url_params)
    if result.success?
      redirect_to stats_url_url(result.url.slug), notice: result.message
    else
      @url = result.url
      flash.alert = result.message
      render :new
    end
  end

  def show
    result = ShowUrl.call(show_params)
    if result.success?
      redirect_to result.url.original_url
    else
      redirect_to new_url_path, alert: result.message
    end
  end

  def stats
    result = GetSingleUrlStats.call(slug_params)
    if result.success?
      @url = result.url
      @stats = result.stats
    else
      redirect_to new_url_path, alert: result.message
    end
  end

  private

  def url_params
    params.require(:url).permit(:original_url, :slug)
  end

  def slug_params
    params.permit(:slug)
  end

  def show_params
    slug_params.merge ip: request.remote_ip
  end
end
