class Event < ApplicationRecord
  AVAILABLE_TYPES = %w[url_entered].freeze

  validates :event_type, presence: true, inclusion: AVAILABLE_TYPES

  attribute :user_data, :json, default: {}

  belongs_to :url
end
