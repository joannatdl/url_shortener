class Url < ApplicationRecord
  validates :original_url, presence: true,
                           url: { no_local: true }
  validates :slug,         presence: true,
                           uniqueness: true,
                           format: { with: /\A[a-zA-Z._-]+\z/ }
  has_many :events

  def enters_count
    events.where(event_type: :url_entered).count
  end
end
