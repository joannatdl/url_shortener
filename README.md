# URL Shortener

Get the application up and running:

- install rvm https://rvm.io/
- `rvm install ruby-2.7.2`
- `rvm 2.7.2@url_shortener --create`
- `gem install bundler`
- `brew install postgresql`
- `brew services start postgresql`
- `git clone https://gitlab.com/joannatdl/url_shortener.git`
- `bundle install`
- `rails db:setup`
- `rails server`


Run tests:

- run `rspec --exclude-pattern "**/{system,features}/*_spec.rb"` to run all but integration tests

- run `rspec` to run all tests

Check tests coverage:

- after running `rspec --exclude-pattern "**/{system,features}/*_spec.rb"` run `open coverage/index.html`

Keep test coverage as high as possible.


Check if your code conforms to the Ruby style guide:

- run `rubocop`
- auto correct `rubocop --auto-correct`




Features:
- Rails application takes a URL and returns a shortened URL
- if a user visits the shortened URL, she is redirected to the full URL
- each time a shortened URL is accessed, the application updates statistics regarding the URL
- a user can view statistics (a counter is enought) of any kind and export it to CSV format
--------
Bonus:
- statistics enrichment using geo-APIs (example: `https://api.ip2country.info/ip?5.6.7.8`)
- __Custom path__ - user may enter desired short URL (/my_cool_url)
- extended statistics for URL owner and regular users (if authentication is implemented)
--------
Out of scope:
- authentication and authorisation (feel free to implement if you have time)
- rich client-side of any kind (JS, complex HTML markup, CSS beautifiers etc.)
- deployment part
- high load optimisation


Improvements:
- we can setup some limits because we are exposing a POST API without requiring to log in
- we can check if the link is returning a 200 before saving it
- we can add new events, for example url_created
- we can use the API for getting the country code from request IP (I mocked it as 'pl' for now)
- we can add remove action
- we can add copy button to get the link
- we can limit slug length
- we can add index on slug in urls table
- we can use i18n to extract the user visible texts to one place
- we can log problems with creating events if there were any
- we can add csv button somewhere to be able to get the csv without typing the url
- we can create stats page to view stats for all the urls (we can view stats for a single url as html now)


Routes:

- `http://localhost:3000/` - add new slug
- `http://localhost:3000/slug_name` - go to slug -> url
- `http://localhost:3000/slug_name/stats` - get stats for single url
- `http://localhost:3000/stats.csv` - get csv stats for all urls
