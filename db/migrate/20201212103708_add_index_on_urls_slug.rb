class AddIndexOnUrlsSlug < ActiveRecord::Migration[6.0]
  def change
    add_index :urls, :slug
  end
end
