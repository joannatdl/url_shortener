class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :event_type
      t.references :url
      t.json :user_data

      t.timestamps
    end
  end
end
