return if Url.any?

10.times do
  Url.create!(
    original_url: Faker::Internet.url,
    slug: Faker::Internet.slug
  )
end
