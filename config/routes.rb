Rails.application.routes.draw do
  resources :stats, only: :index
  resources :urls, param: :slug, path: '', only: %i[new create show] do
    get 'stats', on: :member
  end

  root to: 'urls#new'
end
